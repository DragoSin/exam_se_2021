package dragosin.alex.se.exam.s2;

import javax.swing.*;

public class Subject_2 extends JFrame{
    JTextField file_read,file_write;
    JButton button;

    Subject_2(){

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width=150;int height=300;

        file_read = new JTextField();
        file_read.setBounds(50,50,width, height);

        file_write = new JTextField();
        file_write.setBounds(300,50,width, height);

        width=100;height=50;

        button = new JButton("Button");
        button.setBounds(200,375,width,height);

        add(file_read);add(file_write);add(button);
    }

    public static void main(String[] args){
        Subject_2 window = new Subject_2();
    }
}
