package dragosin.alex.se.exam.s1;

import java.util.ArrayList;

public class Subject_1 {
}

class B{
    String param;

    void x(){}
    void y(){}
}

class A extends B{}

class C{
    B b;
}

class D{
    private B b;
}

class E{
    private ArrayList<B> b = new ArrayList<>();
}

class U{
    public void x(){}
}